//
//  main.m
//  testGalery
//
//  Created by Sergey Kim on 02.06.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

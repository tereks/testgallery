//
//  ViewController.m
//  testGalery
//
//  Created by Sergey Kim on 02.06.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ViewController.h"
#import "iCarousel.h"
#import "Settings.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController () <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) IBOutlet UIGestureRecognizer * tapRecognizer;

@property (nonatomic, strong) IBOutlet UIView * commentContainer;
@property (nonatomic, strong) IBOutlet UITextField * commentField;

@property (nonatomic, strong) NSTimer * timer;
@end

@implementation ViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _commentContainer.alpha = 0;
    
    [self loadImages];
}

- (void) loadImages {
    NSError *deserializingError;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Images" ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    id object = [NSJSONSerialization JSONObjectWithData:myData
                                                options:NSJSONReadingAllowFragments
                                                  error:&deserializingError];
    
    if ( object ) {
        _items = [[NSMutableArray alloc] initWithArray:object];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [self applySettings];
    [_carousel reloadData];
}

- (void) applySettings {
    _carousel.type = [[Settings instance] animationType];
    [_timer invalidate];
    _timer = nil;
    
    if ( [[Settings instance] autoScroll] ) {
        [self startTimer];
    }
    
    if ( [[Settings instance] showFavouriteImages] ) {
        _items = (NSMutableArray*)[_items filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            NSDictionary * imageItem = evaluatedObject;
            NSString * idStr = [[Settings instance] titleForIndex:[NSString stringWithFormat:@"%@", imageItem[@"id"]]];
            if ( [idStr length] > 0 ) {
                return YES;
            }
            return NO;
        } ] ];
    }
    else {
        _items = nil;
        [self loadImages];
    }
    
    if ( [[Settings instance] showRandomly] ) {
        [self randomizeArray];
    }
    else {
        _items = (NSMutableArray*)[_items sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSDictionary* obj1, NSDictionary* obj2) {
            return [obj1[@"order"] compare:obj2[@"order"]];
        }];
    }
}

- (void) startTimer {
    _timer = [NSTimer scheduledTimerWithTimeInterval:[[Settings instance] timerValue]
                                              target:self
                                            selector:@selector(timerTick)
                                            userInfo:nil
                                             repeats:YES];
}

- (void) timerTick {
    NSUInteger nextIndex = MIN( _carousel.currentItemIndex+1, [_carousel numberOfItems]);
    [_carousel scrollToItemAtIndex:nextIndex animated:YES];
}

- (void) randomizeArray {
    for (int i = (int)(_items.count-1); i > 0; i--)
    {
        [_items exchangeObjectAtIndex:i withObjectAtIndex:arc4random_uniform(i+1)];
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return [_items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.9, self.view.frame.size.height*0.8)];
        view.contentMode = UIViewContentModeScaleAspectFit;
        view.backgroundColor = [UIColor clearColor];
        
        CGFloat y = view.frame.size.height - view.frame.size.height*0.3;
        label = [[UILabel alloc] initWithFrame:(CGRect){0, y, view.frame.size.width, view.frame.size.height*0.3}];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:17];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        label = (UILabel *)[view viewWithTag:1];
    }

    NSString * idStr = [NSString stringWithFormat:@"%@", _items[index][@"id"]];
    label.text = [[Settings instance] titleForIndex:idStr];
    
    NSString * imageUrlStr = _items[index][@"url"];
    [(UIImageView*)view sd_setImageWithURL:[NSURL URLWithString:[imageUrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                   options:SDWebImageRetryFailed];
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    BOOL showKeyboard = !_commentContainer.alpha;
    
    [_timer invalidate];
    
    [self setCommentViewVisible:showKeyboard];
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel {
    [self setCommentViewVisible:NO];
}

#pragma mark -
#pragma mark UITextFieldDelegate methods
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self setCommentViewVisible:NO];
    
    NSString * idStr = [NSString stringWithFormat:@"%@", _items[_carousel.currentItemIndex][@"id"]];
    [[Settings instance] setTitle:textField.text forIndex:idStr];
    
    [_carousel reloadData];
    if ( [[Settings instance] autoScroll] ) {
        [self startTimer];
    }
    return YES;
}

- (void) setCommentViewVisible:(BOOL)visible {
    [UIView animateWithDuration:0.2 animations:^{
        _commentContainer.alpha = visible;
    }];
    
    if ( visible ) {
        [_commentField becomeFirstResponder];
    }
    else {
        [_commentField resignFirstResponder];
    }
}

@end

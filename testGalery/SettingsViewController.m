//
//  SettingsViewController.m
//  testGalery
//
//  Created by Sergey Kim on 02.06.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "SettingsViewController.h"
#import "Settings.h"
#import "iCarousel.h"

@interface SettingsViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel * timerLabel;
@property (nonatomic, strong) IBOutlet UISlider * slider;
@property (nonatomic, strong) IBOutlet UISwitch * autoSwitch;
@property (nonatomic, strong) IBOutlet UISegmentedControl * showFavourites;
@property (nonatomic, strong) IBOutlet UISegmentedControl * showOrdered;
@property (nonatomic, strong) IBOutlet UITextField * animationTypeField;
@property (nonatomic, strong) IBOutlet UIPickerView * animationPicker;

@property (nonatomic, strong) IBOutlet NSDictionary * animationsDict;
@end

@implementation SettingsViewController {
    NSInteger animationIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _animationsDict = @{
                       @(iCarouselTypeLinear) : @"Linear",
                       @(iCarouselTypeInvertedWheel) : @"InvertedWheel",
                       @(iCarouselTypeCoverFlow) : @"CoverFlow",
                       @(iCarouselTypeTimeMachine) : @"TimeMachine",
                       };
    
    [_slider setValue:[[Settings instance] timerValue]];
    [self sliderValueChanges:_slider];
    [_autoSwitch setOn:[[Settings instance] autoScroll]];

    [_showFavourites setSelectedSegmentIndex:(int)[[Settings instance] showFavouriteImages]];
    [_showOrdered setSelectedSegmentIndex:[[Settings instance] showRandomly]];
    
    _animationTypeField.text = _animationsDict[@([[Settings instance] animationType])];
    
    _animationPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 100, 150)];
    [_animationPicker setDataSource: self];
    [_animationPicker setDelegate: self];
    _animationPicker.showsSelectionIndicator = YES;
    self.animationTypeField.inputView = _animationPicker;
    
    UIToolbar * toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)] ;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIBarButtonItem *btnSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)] ;
    btnDone.title = @"Готово";
    toolbar.items = @[btnSpace, btnDone];
    self.animationTypeField.inputAccessoryView = toolbar;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[Settings instance] setTimerValue          :(int)_slider.value];
    [[Settings instance] setShowFavouriteImages :(BOOL)_showFavourites.selectedSegmentIndex];
    [[Settings instance] setShowRandomly        :(BOOL)_showOrdered.selectedSegmentIndex];
    [[Settings instance] setAutoScroll          :_autoSwitch.isOn];
    [[Settings instance] setAnimationType       :animationIndex];
    
    [[Settings instance] saveSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sliderValueChanges:(id)sender {
    UISlider * slider = (UISlider*)sender;
    _timerLabel.text = [NSString stringWithFormat:@"%d", (int)[slider value]];
}

- (void) done:(id)sender {
    [_animationTypeField resignFirstResponder];
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 4;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[_animationsDict allValues] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _animationTypeField.text = [[_animationsDict allValues] objectAtIndex:row];
    
    animationIndex = [[[_animationsDict allKeys] objectAtIndex:row] integerValue];
}
@end

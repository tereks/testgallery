//
//  Settings.h
//  testGalery
//
//  Created by Sergey Kim on 02.06.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

@property (nonatomic, assign) NSUInteger timerValue;
@property (nonatomic, assign) BOOL autoScroll;
@property (nonatomic, assign) BOOL showFavouriteImages;
@property (nonatomic, assign) BOOL showRandomly;
@property (nonatomic, assign) NSInteger animationType;

+ (id) instance;
- (void) saveSettings;

- (void) setTitle:(NSString*)title forIndex:(NSString*)index;
- (NSString*) titleForIndex:(NSString*)index;
@end

//
//  Settings.m
//  testGalery
//
//  Created by Sergey Kim on 02.06.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "Settings.h"

#define FAVOURITES @"favourites"
#define TIMER_INTERVAL @"timer_interval"
#define AUTO_SCROLL @"auto_scroll"
#define SHOW_FAVOURITES_IMAGES @"show_favourites"
#define SHOW_RANDOMLY @"show_randomply"
#define ANIMATION_TYPE @"animation_type"

@interface Settings()

@property (nonatomic, strong) NSMutableDictionary * titlesDict;

@end

@implementation Settings

+ (id) instance {
    static Settings *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self loadSettings];
    }
    return self;
}

- (void) loadSettings {
    _titlesDict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:FAVOURITES]];
    
    _timerValue = MAX( [[[NSUserDefaults standardUserDefaults] objectForKey:TIMER_INTERVAL] integerValue], 1 );
    _autoScroll = [[NSUserDefaults standardUserDefaults] boolForKey:AUTO_SCROLL] ;
    
    _showFavouriteImages = [[NSUserDefaults standardUserDefaults] boolForKey:SHOW_FAVOURITES_IMAGES] ;
    _showRandomly = [[NSUserDefaults standardUserDefaults] boolForKey:SHOW_RANDOMLY] ;
    
    _animationType = [[[NSUserDefaults standardUserDefaults] objectForKey:ANIMATION_TYPE] integerValue];
}

- (void) saveSettings {
    [[NSUserDefaults standardUserDefaults] setBool:_showRandomly forKey:SHOW_RANDOMLY];
    [[NSUserDefaults standardUserDefaults] setBool:_showFavouriteImages forKey:SHOW_FAVOURITES_IMAGES];
    [[NSUserDefaults standardUserDefaults] setBool:_autoScroll forKey:AUTO_SCROLL];
    [[NSUserDefaults standardUserDefaults] setObject:@(_timerValue) forKey:TIMER_INTERVAL];
    [[NSUserDefaults standardUserDefaults] setObject:@(_animationType) forKey:ANIMATION_TYPE];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setTitle:(NSString*)title forIndex:(NSString*)index {
    _titlesDict[index] = title;
    
    [[NSUserDefaults standardUserDefaults] setObject:_titlesDict forKey:FAVOURITES];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*) titleForIndex:(NSString*)index {
    return _titlesDict[index];
}

@end
